<?php


/**
 *  @file
 *  This defines views hooks for the Nodepoints module. It will be loaded automatically as needed by the Views module.
 */

/**
 * Implements hook_views_data().
 */
function nodepoints_views_data() {
  // ----------------------------------------------------------------
  // nodepoints table
  // Describe the nodepoints table.
  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['nodepoints']['table']['group'] = t('Nodepoints');

  $data['nodepoints']['table']['base'] = array(
      'field' => 'nid',
      'title' => t('Nodepoints'),
      'help' => t('Points by category accumulated by nodes on your site.', nodepoints_translation()),
  );

  $data['nodepoints']['table']['join'] = array(
      'node' => array(
          'left_field' => 'nid',
          'field' => 'nid',
      ),
      'taxonomy_term_data' => array(
          'left_field' => 'tid',
          'field' => 'tid',
      ),
      // This goes to the node so that we have consistent authorship.
      'node_revisions' => array(
          'left_field' => 'nid',
          'field' => 'nid',
      ),
  );

  // Describe the points column of the nodepoints table.
  $data['nodepoints']['points'] = array(
      'title' => t('Current !points in category', nodepoints_translation()),
      'help' => t("A Node's current !points in a single category.", nodepoints_translation()), // The help that appears on the UI,
      'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE,
      ),
      'argument' => array(
          'handler' => 'views_handler_argument_numeric',
          'numeric' => TRUE,
          'name field' => 'points', // display this field in the summary
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
  );

  // Describe the tid column of the nodepoints table.
  $data['nodepoints']['tid'] = array(
      'title' => t('Points category'),
      'help' => t('The categories (terms) of nodepoints used'), // The help that appears on the UI,
      'field' => array(
          'handler' => 'nodepoints_views_handler_field_category',
      ),
      'argument' => array(
          'handler' => 'nodepoints_views_handler_argument_category',
          'numeric' => TRUE,
          'name field' => 'category', // display this field in the summary
      ),
      'filter' => array(
          'handler' => 'nodepoints_views_handler_filter_category',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
  );

  // Describe the max_points column of the nodepoints table.
  $data['nodepoints']['max_points'] = array(
      'title' => t('Max !points in category', nodepoints_translation()),
      'help' => t("A Node's max !points in a single category.", nodepoints_translation()), // The help that appears on the UI,
      'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE,
      ),
      'argument' => array(
          'handler' => 'views_handler_argument_numeric',
          'numeric' => TRUE,
          'name field' => 'max_points', // display this field in the summary
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
  );

  // Describe the last_update column of the nodepoints table.
  $data['nodepoints']['last_update'] = array(
      'title' => t('Last update in category'),
      'help' => t("The last update timestamp for a Node's current !points in a single category.", nodepoints_translation()),
      'field' => array(
          'handler' => 'views_handler_field_date',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort_date',
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_date',
      ),
  );

  // Add relationship to node table.
  $data['nodepoints']['nid'] = array(
      'title' => t('Node'),
      'help' => t('Relate the nodepoints table to the node table.'),
      'relationship' => array(
          'base' => 'node',
          'field' => 'nid',
          'label' => t('Nodes'),
          'handler' => 'views_handler_relationship',
      ),
  );

  // ----------------------------------------------------------------
  // nodepoints_total table
  // Describe the nodepoints_total table.
  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['nodepoints_total']['table']['group'] = t('Nodepoints total');

  $data['nodepoints_total']['table']['base'] = array(
      'field' => 'nid',
      'title' => t('Nodepoints Total'),
      'help' => t('Total !points over all categories accumulated by nodes on your site.', nodepoints_translation()),
  );

  $data['nodepoints_total']['table']['join'] = array(
      'node' => array(
          'left_field' => 'nid',
          'field' => 'nid',
      ),
      'taxonomy_term_data' => array(
          'left_field' => 'tid',
          'field' => 'tid',
      ),
      // This goes to the node so that we have consistent authorship.
      'node_revisions' => array(
          'left_field' => 'nid',
          'field' => 'nid',
      ),
  );

  // Describe the points column of the nodepoints_total table.
  $data['nodepoints_total']['points'] = array(
      'title' => t('Current total !points', nodepoints_translation()),
      'help' => t("A Node's current !points across all categories.", nodepoints_translation()), // The help that appears on the UI,
      'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE,
      ),
      'argument' => array(
          'handler' => 'views_handler_argument_numeric',
          'numeric' => TRUE,
          'name field' => 'points', // display this field in the summary
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
  );

  // Describe the max_points column of the nodepoints_total table.
  $data['nodepoints_total']['max_points'] = array(
      'title' => t('Max total !points', nodepoints_translation()),
      'help' => t("A node's max !points across all categories.", nodepoints_translation()), // The help that appears on the UI,
      'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE,
      ),
      'argument' => array(
          'handler' => 'views_handler_argument_numeric',
          'numeric' => TRUE,
          'name field' => 'max_points', // display this field in the summary
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
  );

  // Describe the last_update column of the nodepoints_total table.
  $data['nodepoints_total']['last_update'] = array(
      'title' => t('Last update of total !points', nodepoints_translation()),
      'help' => t("The last update timestamp for a node's !points across all categories.", nodepoints_translation()),
      'field' => array(
          'handler' => 'views_handler_field_date',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort_date',
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_date',
      ),
  );

  // Add relationship to node table.
  $data['nodepoints_total']['nid'] = array(
      'title' => t('Node'),
      'help' => t('Relate the nodepoints total table to the node table.'),
      'relationship' => array(
          'base' => 'node',
          'field' => 'nid',
          'label' => t('Node'),
          'handler' => 'views_handler_relationship',
      ),
  );

  // ----------------------------------------------------------------
  // nodepoints_txn table
  // Describe the nodepoints_txn table.
  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['nodepoints_txn']['table']['group'] = t('Nodepoints Transactions');

  $data['nodepoints_txn']['table']['base'] = array(
      'field' => 'txn_id',
      'title' => t('Nodepoints Transactions'),
      'help' => t('Points transactions accumulated by nodes on your site.', nodepoints_translation()),
  );

  $data['nodepoints_txn']['table']['join'] = array(
      'node' => array(
          'left_field' => 'nid',
          'field' => 'nid',
      ),
      'taxonomy_term_data' => array(
          'left_field' => 'tid',
          'field' => 'tid',
      ),
      // This goes to the node so that we have consistent authorship.
      'node_revisions' => array(
          'left_field' => 'nid',
          'field' => 'nid',
      ),
  );

  // Describe the points column of the nodepoints table.
  $data['nodepoints_txn']['points'] = array(
      'title' => t('Points', nodepoints_translation()),
      'help' => t("A Node's !points for this transaction.", nodepoints_translation()), // The help that appears on the UI,
      'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE,
      ),
      'argument' => array(
          'handler' => 'views_handler_argument_numeric',
          'numeric' => TRUE,
          'name field' => 'points', // display this field in the summary
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
  );

  // Describe the tid column of the nodepoints table.
  $data['nodepoints_txn']['tid'] = array(
      'title' => t('Category'),
      'help' => t('The categories (terms) of nodepoints used for this transaction'), // The help that appears on the UI,
      'field' => array(
          'handler' => 'nodepoints_views_handler_field_category',
      ),
      'argument' => array(
          'handler' => 'nodepoints_views_handler_argument_category',
          'numeric' => TRUE,
          'name field' => 'category', // display this field in the summary
      ),
      'filter' => array(
          'handler' => 'nodepoints_views_handler_filter_category',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
  );

  // Add relationship to node table.
  $data['nodepoints_txn']['nid'] = array(
      'title' => t('Node'),
      'help' => t('Relate the nodepoints table to the node table.'),
      'argument' => array(
          'handler' => 'views_handler_argument_numeric',
          'numeric' => TRUE,
      ),
      'relationship' => array(
          'base' => 'node',
          'field' => 'nid',
          'label' => t('Node'),
          'handler' => 'views_handler_relationship',
      ),
  );

  $data['nodepoints_txn']['time_stamp'] = array(
      'title' => t('Timestamp'),
      'help' => t('The created timestamp for the transaction.'),
      'field' => array(
          'handler' => 'views_handler_field_date',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort_date',
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_date',
      ),
  );

  $data['nodepoints_txn']['changed'] = array(
      'title' => t('Changed'),
      'help' => t('The changed timestamp for the transaction, for when the transaction is updated.'),
      'field' => array(
          'handler' => 'views_handler_field_date',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort_date',
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_date',
      ),
  );

  $data['nodepoints_txn']['status'] = array(
      'title' => t('Status'),
      'help' => t('The status of the transaction.'),
      'field' => array(
          'handler' => 'views_handler_field_numeric',
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
  );

  $data['nodepoints_txn']['description'] = array(
      'title' => t('Description'),
      'help' => t('The description for the transaction.'),
      'field' => array(
          'handler' => 'views_handler_field',
      ),
  );

  $data['nodepoints_txn']['reference'] = array(
      'title' => t('Reference'),
      'help' => t('The reference for the transaction.'),
      'field' => array(
          'handler' => 'views_handler_field',
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_string',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
  );

  $data['nodepoints_txn']['expirydate'] = array(
      'title' => t('Expiry date'),
      'help' => t('The expiration date for the transaction.'),
      'field' => array(
          'handler' => 'views_handler_field_date',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort_date',
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_date',
      ),
  );

  $data['nodepoints_txn']['expired'] = array(
      'title' => t('Expired'),
      'help' => t('The expiry status for the transaction.'),
      'field' => array(
          'handler' => 'views_handler_field_numeric',
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
  );

  $data['nodepoints_txn']['entity_id'] = array(
      'title' => t('Entity ID'),
      'help' => t('The entity_id field. Used to relate to the node table.'),
      'field' => array(
          'handler' => 'views_handler_field',
      ),
      'argument' => array(
          'handler' => 'views_handler_argument_numeric',
          'numeric' => TRUE,
      ),
      'relationship' => array(
          'base' => 'node',
          'field' => 'nid',
          'label' => t('Entity'),
          'handler' => 'views_handler_relationship',
      ),
  );

  $data['nodepoints_txn']['entity_type'] = array(
      'title' => t('Entity type'),
      'help' => t('The entity type for the transaction.'),
      'field' => array(
          'handler' => 'views_handler_field',
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_string',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
  );

  $data['nodepoints_txn']['operation'] = array(
      'title' => t('Operation'),
      'help' => t('The operation for the transaction.'),
      'field' => array(
          'handler' => 'views_handler_field',
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_string',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
  );

  // Reverse join information. This should not be necessary.
  $data['taxonomy_term_data']['table']['join'] = array(
    'nodepoints' => array(
      'left_field' => 'tid',
      'field' => 'tid',
    ),
    'nodepoints_txn' => array(
      'left_field' => 'tid',
      'field' => 'tid',
    ),
  );

  return $data;
}
