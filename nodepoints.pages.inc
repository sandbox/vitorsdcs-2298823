<?php

/**
 * @file
 * Menu callbacks for nodepoints.module.
 */


/**
 * Displays a detailed transaction report for an individual node.
 *
 * @param $node
 *   For which node to display.
 */
function nodepoints_list_transactions($form, &$form_state, $node, $tid = NULL) {

  // If this is an AJAX request, update $_GET['q'] so that table sorting and
  // similar links are using the correct base path.
  if ($_GET['q'] == 'system/ajax') {
    $_GET['q'] = 'node/' . $node->nid . '/points';
  }

  $settings = array(
    'show_node' => FALSE,
  );
  $header = nodepoints_get_transaction_header($settings);

  $query = db_select('nodepoints_txn', 'p')->extend('PagerDefault')->extend('TableSort')
    ->fields('p')
    ->condition('p.nid', $node->nid)
    ->orderByHeader($header)
    // Enforce consistent sort order.
    ->orderBy('p.txn_id', 'DESC')
    ->limit(variable_get(NODEPOINTS_REPORT_LIMIT, 10));

  if (module_exists('taxonomy')) {
    $query->leftJoin('taxonomy_term_data', 't', 'p.tid = t.tid');
  }

  $unapproved_query = db_select('nodepoints_txn', 'p')
    ->condition('nid', $node->nid)
    ->condition('status', NODEPOINTS_TXN_STATUS_PENDING);
  $unapproved_query->addExpression('SUM(points)');

  $values = nodepoints_filter_parse_input($form_state, $tid);
  $active_category = nodepoints_filter_query($query, $values);
  nodepoints_filter_query($unapproved_query, $values);

  if (isset($active_category)) {
    drupal_set_title(t('!Points for @node_title (%category category)', nodepoints_translation() + array('%category' => $active_category, '@node_title' => $node->title)), PASS_THROUGH);
    $total_title = t('Total !points (%category category)', nodepoints_translation() + array('%category' => $active_category));
  }
  else {
    drupal_set_title(t('!Points for @node_title', nodepoints_translation() + array('@node_title' => $node->title)));
    $total_title = t('Total !points', nodepoints_translation());
  }

  $rows = array();
  foreach ($query->execute() as $transaction) {
    $rows[] = nodepoints_get_transaction_row($transaction, $settings);
  }

  // Store context in the output array so that modules have access to it.
  $output = array(
    '#node' => $node,
    '#attached' => array(
      'css' => array(
        drupal_get_path('module', 'nodepoints') . '/nodepoints.css',
      ),
    ),
  );

  $output['form'] = nodepoints_filter_form($node, $values);

  $output['list'] = array(
    '#type' => 'container',
    '#id' => 'nodepoints_list_wrapper',
  );
  $output['list']['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No !Points earned', nodepoints_translation()),
    '#weight' => -5,
    '#attributes' => array('class' => array('nodepoints-mynodepoints-list')),
  );
  $output['list']['pager'] = array(
    '#markup' => theme('pager'),
    '#weight' => 0,
  );

  // Fetch pending (not yet approved) points according to the category filter.
  $pending = (int)$unapproved_query
    ->execute()
    ->fetchField();

  // Display both pending and approved points in a simple table.
  $output['list']['summary_table'] = array(
    '#theme' => 'table',
    '#header' => array(
      array(
        'data' => $total_title,
        'colspan' => 2,
      ),
    ),
    '#rows' => array(
      array(
        'data' => array(t('Approved !points', nodepoints_translation()), nodepoints_get_current_points($node->nid, isset($values['tid']) ? $values['tid'] : 'all')),
        'class' => array('nodepoints-mynodepoints-total-approved'),
      ),
      array(
        'data' => array(t('Pending !points', nodepoints_translation()), $pending),
        'class' => array('nodepoints-mynodepoints-total-pending'),
      ),
    ),
    '#weight' => 10,
    '#attributes' => array('class' => array('nodepoints-mynodepoints-total')),
  );

  // For simplicity, the generated output is passed to a custom alter function.
  // This would also be possible through hook_page_alter(), but that hook is
  // hard to use.
  drupal_alter('nodepoints_list_transactions', $output);

  return $output;
}


/**
 * Lists the nodes and their point totals by all or by category.
 */
function nodepoints_list_nodes($form, &$form_state, $tid = NULL) {

  // If this is an AJAX request, update $_GET['q'] so that table sorting and
  // similar links are using the correct base path.
  if ($_GET['q'] == 'system/ajax') {
    $_GET['q'] = 'nodepoints';
  }

  $header = nodepoints_get_list_header();

  $query = db_select('nodepoints', 'p')->extend('PagerDefault')->extend('TableSort')
    ->fields('p', array('nid', 'points', 'tid'))
    ->fields('n', array('title'))
    ->groupBy('p.nid')
    ->groupBy('u.name')
    ->groupBy('p.points')
    ->groupBy('p.tid')
    ->orderByHeader($header)
    ->limit(variable_get(NODEPOINTS_REPORT_NODECOUNT, 30));

  $query->join('node', 'n', 'p.nid = n.nid');
  if (module_exists('taxonomy')) {
    $query->groupBy('t.name');
    $query->leftJoin('taxonomy_term_data', 't', 'p.tid = t.tid');
  }

  $values = nodepoints_filter_parse_input($form_state, $tid);
  $active_category = nodepoints_filter_query($query, $values);

  if (isset($active_category)) {
    drupal_set_title(t('All points (%category category)', nodepoints_translation() + array('%category' => $active_category)), PASS_THROUGH);
  }
  else {
    drupal_set_title(t('All points'));
  }

  if (variable_get(NODEPOINTS_REPORT_DISPLAYZERO, 1) == 0) {
    // The user would NOT like to see nodes with zero points.
    $query->condition('p.points', 0, '<>');
  }

  $rows = array();
  foreach ($query->execute() as $data) {
    $rows[] = nodepoints_get_list_row($data);
  }

  $output = array();
  $output['form'] = nodepoints_filter_form(NULL, $values);
  $output['list'] = array(
    '#type' => 'container',
    '#id' => 'nodepoints_list_wrapper',
  );
  $output['list']['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
  $output['list']['pager'] = array(
    '#theme' => 'pager',
  );

  // Allow other modules and themes to customize the result.
  drupal_alter('nodepoints_list', $output);

  return $output;
}

/**
 * Menu callback; display details about a specific transaction.
 *
 * @param $transaction
 *   Transaction object.
 * @return
 *   Render-able array with all the information about this transaction.
 */
function nodepoints_view_transaction($transaction) {
  drupal_add_css(drupal_get_path('module', 'nodepoints') . '/nodepoints.css');

  drupal_set_title(t('View transaction #@txn_id', array('@txn_id' => $transaction->txn_id)));

  $css_stati = array(
    NODEPOINTS_TXN_STATUS_APPROVED => 'approved',
    NODEPOINTS_TXN_STATUS_DECLINED => 'declined',
    NODEPOINTS_TXN_STATUS_PENDING => 'pending',
  );
  $classes = 'nodepoints-view-' . $css_stati[$transaction->status] . ' nodepoints-view-category-' . $transaction->tid . ' nodepoints-view-' . ($transaction->points > 0 ? 'positive' : 'negative');
  if (!empty($transaction->expirydate)) {
    $classes .= $transaction->expired ? ' nodepoints-view-expired' : ' nodepoints-view-not-expired';
  }

  $content = array(
    '#prefix' => '<div class="nodepoints-view-transaction ' . $classes . '">',
    '#suffix' => '</div>',
  );

  $content['details'] = array(
    '#theme' => 'nodepoints_view_category',
    '#title' => t('Details'),
    '#weight' => 0,
    '#attributes' => array('class' => array('nodepoints-group-details')),
  );

  $content['details']['node'] = array(
    '#theme' => 'nodepoints_view_item',
    '#title' => t('Node'),
    '#value' => $transaction->node->title,
    '#weight' => 0,
    '#attributes' => array('class' => array('nodepoints-item-node')),
  );

  $content['details']['points'] = array(
    '#theme' => 'nodepoints_view_item',
    '#title' => t('!Points', nodepoints_translation()),
    '#value' => $transaction->points,
    '#weight' => 10,
    '#attributes' => array('class' => array('nodepoints-item-points')),
  );

  $content['details']['category'] = array(
    '#theme' => 'nodepoints_view_item',
    '#title' => t('Category'),
    '#value' => $transaction->category,
    '#weight' => 20,
    '#attributes' => array('class' => array('nodepoints-item-category')),
  );

  $content['details']['reason'] = array(
    '#theme' => 'nodepoints_view_item',
    '#title' => t('Reason'),
    '#value' => nodepoints_create_description($transaction, array('truncate' => FALSE)),
    '#weight' => 30,
    '#attributes' => array('class' => array('nodepoints-item-reason')),
  );

  $content['details']['transaction'] = array(
    '#theme' => 'nodepoints_view_item',
    '#title' => t('Transaction ID'),
    '#value' => $transaction->txn_id,
    '#weight' => 40,
    '#attributes' => array('class' => array('nodepoints-item-transaction')),
  );

  $content['status'] = array(
    '#theme' => 'nodepoints_view_category',
    '#title' => t('Status'),
    '#weight' => 10,
    '#attributes' => array('class' => array('nodepoints-group-status')),
  );

  $stati = nodepoints_txn_status();
  $content['status']['status'] = array(
    '#theme' => 'nodepoints_view_item',
    '#title' => t('Approval status'),
    '#value' => $stati[$transaction->status],
    '#weight' => 0,
    '#attributes' => array('class' => array('nodepoints-item-status')),
  );

  $content['status']['date'] = array(
    '#theme' => 'nodepoints_view_item',
    '#title' => t('Creation date'),
    '#value' => format_date($transaction->time_stamp),
    '#weight' => 10,
    '#attributes' => array('class' => array('nodepoints-item-date')),
  );

  $content['status']['changed'] = array(
    '#theme' => 'nodepoints_view_item',
    '#title' => t('Last modified'),
    '#value' => format_date($transaction->changed),
    '#weight' => 20,
    '#attributes' => array('class' => array('nodepoints-item-changed')),
  );

  if (!empty($transaction->expirydate)) {
    $content['status']['expiration_status'] = array(
      '#theme' => 'nodepoints_view_item',
      '#title' => t('Expiration status'),
      '#value' => $transaction->expired ? t('Expired') : t('Not expired'),
      '#weight' => 20,
      '#attributes' => array('class' => array('nodepoints-item-expiration-status')),
    );
    $content['status']['expiration_date'] = array(
      '#theme' => 'nodepoints_view_item',
      '#title' => t('Expiration date'),
      '#value' => format_date($transaction->expirydate),
      '#weight' => 30,
      '#attributes' => array('class' => array('nodepoints-item-points-expiration-date')),
    );
  }

  if (!empty($transaction->parent_txn_id)) {
    $parent_transaction = nodepoints_transaction_load($transaction->parent_txn_id);
    $parent = l(nodepoints_create_description($parent_transaction, array('link' => FALSE)), 'nodepoints/view/' . $transaction->parent_txn_id, array('html' => TRUE));
  }

  $child_txn_ids = db_query('SELECT txn_id FROM {nodepoints_txn} WHERE parent_txn_id = :txn_id', array(':txn_id' => $transaction->txn_id))->fetchCol();
  $children = array();
  foreach ($child_txn_ids as $child_txn_id) {
    $child_transaction = nodepoints_transaction_load($child_txn_id);
    $children[] = l(nodepoints_create_description($child_transaction, array('link' => FALSE)), 'nodepoints/view/' . $child_txn_id, array('html' => TRUE));
  }
  $children = !empty($children) ? theme('item_list', array('items' => $children)) : '';

  if (!empty($parent) || !empty($children)) {
    $content['related'] = array(
      '#theme' => 'nodepoints_view_category',
      '#title' => t('Related !points transactions', nodepoints_translation()),
      '#weight' => 20,
      '#attributes' => array('class' => array('nodepoints-group-related')),
    );

    if (!empty($parent)) {
      $content['related']['parent'] = array(
        '#theme' => 'nodepoints_view_item',
        '#title' => t('Prior transaction'),
        '#value' => $parent,
        '#weight' => 0,
        '#attributes' => array('class' => array('nodepoints-item-parent')),
      );
    }

    if (!empty($children)) {
      $content['related']['children'] = array(
        '#theme' => 'nodepoints_view_item',
        '#title' => t('Follow-up transactions'),
        '#value' => $children,
        '#weight' => 10,
        '#attributes' => array('class' => array('nodepoints-item-children')),
      );
    }
  }

  if (nodepoints_admin_access('edit')) {
    $content['admin'] = array(
      '#theme' => 'nodepoints_view_category',
      '#title' => t('Admin'),
      '#weight' => 30,
      '#attributes' => array('class' => array('nodepoints-group-admin')),
    );

    if (!empty($transaction->approver_uid)) {
      $content['admin']['moderator'] = array(
        '#theme' => 'nodepoints_view_item',
        '#title' => t('Moderator'),
        '#value' => theme('username', array('account' => user_load($transaction->approver_uid))),
        '#weight' => 0,
        '#attributes' => array('class' => array('nodepoints-item-moderator')),
      );
    }

    if (!empty($transaction->description)) {
      $content['admin']['description_manual'] = array(
        '#theme' => 'nodepoints_view_item',
        '#title' => t('Description (manually entered)'),
        '#value' => $transaction->description,
        '#weight' => 10,
        '#attributes' => array('class' => array('nodepoints-item-description-manual')),
      );

      $content['admin']['description_generated'] = array(
        '#theme' => 'nodepoints_view_item',
        '#title' => t('Description (auto generated)'),
        '#value' => nodepoints_create_description($transaction, array('skip_description' => TRUE, 'truncate' => FALSE)),
        '#weight' => 20,
        '#attributes' => array('class' => array('nodepoints-item-description-generated')),
      );
    }

    $content['admin']['operation'] = array(
      '#theme' => 'nodepoints_view_item',
      '#title' => t('Operation'),
      '#value' => $transaction->operation,
      '#weight' => 30,
      '#attributes' => array('class' => array('nodepoints-item-operation')),
    );

    if (!empty($transaction->reference)) {
      $content['admin']['reference'] = array(
        '#theme' => 'nodepoints_view_item',
        '#title' => t('Internal reference'),
        '#value' => $transaction->reference,
        '#weight' => 40,
        '#attributes' => array('class' => array('nodepoints-item-reference')),
      );
    }

    $content['admin']['actions'] = array(
      '#theme' => 'nodepoints_view_item',
      '#title' => t('Actions'),
      '#value' => nodepoints_get_transaction_actions($transaction, FALSE),
      '#weight' => 50,
      '#attributes' => array('class' => array('nodepoints-item-actions')),
    );
  }
  return $content;
}