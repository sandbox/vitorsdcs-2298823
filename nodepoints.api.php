<?php

/**
 * @file
 * API documentation for nodepoints.module
 */

/**
 * Return information about registered operations.
 *
 * Modules can register operation strings
 *
 * @return
 *   An array whose keys are operation strings used in
 *   nodepoints_nodepoints_api() and the which has the following properties:
 *   - description: A string that is used as a reason when transactions are
 *     displayed. Either this or a callback (see below) is required.
 *   - description callback: If the reason is dynamic, because he for example
 *     includes the title of a node, a callback function can be given which
 *     receives the transaction object and (if existing) and entity object as
 *     arguments.
 *   - admin description: A description which is searched for and displayed in
 *     the operation autocomplete field in the add points form.
 *
 */
function hook_nodepoints_info() {
  return array(
    'expiry' => array(
      'description' => t('!Points have expired.', nodepoints_translation()),
      'admin description' => t('Expire an existing transaction'),
    )
  );
}

/**
 * Allows to customize the output of a the nodes by points page.
 *
 * @param $output
 *   Render array with the content.
 *
 * @see nodepoints_list_nodes().
 */
function hook_nodepoints_list_alter(&$output) {

}