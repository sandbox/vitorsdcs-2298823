<?php

/**
 * @file
 * Admin menu callbacks for nodepoints.module.
 */

function nodepoints_confirm_approve_submit($form, &$form_state) {
  global $user;

  $params = array(
    'txn_id' => $form_state['values']['txn_id'],
    'approver_uid' => $user->uid,
    'status' => $form_state['values']['operation'],
  );
  nodepoints_nodepointsapi($params);

  $form_state['redirect'] = 'admin/config/content/nodepoints/moderate';
}

/**
 * Form builder for add/edit nodepoints transaction form.
 */
function nodepoints_admin_txn($form, &$form_state, $mode, $txn = NULL) {

  drupal_add_css(drupal_get_path('module', 'nodepoints') . '/nodepoints.css');

  $timestamp = format_date(REQUEST_TIME, 'custom', 'Y-m-d H:i O');
  if ($mode == 'edit') {
    drupal_set_title(t('Edit !points transaction', nodepoints_translation()));
    $timestamp = format_date($txn->time_stamp, 'custom', 'Y-m-d H:i:s O');
    $txn_node = $txn->node;

    $form['txn'] = array(
      '#type' => 'value',
      '#value' => $txn,
    );
  }
  elseif ($mode == 'add') {
    drupal_set_title(t('Add !points', nodepoints_translation()));
    if ($txn) {
      $txn_node = node_load($txn);
    }
  }
  $form['txn_node'] = array(
      '#type' => 'textfield',
      '#title' => t('Node ID'),
      '#size' => 30,
      '#maxlength' => 60,
      '#default_value' => isset($txn_node) ? $txn_node->nid : '',
      '#description' => t('The nid of the node that should gain or lose !points.', nodepoints_translation()),
      '#required' => TRUE,
      '#weight' => -20,
      '#disabled' => $mode == 'edit',
  );

  $form['points'] = array(
      '#type' => 'textfield',
      '#title' => t('Points'),
      '#size' => 10,
      '#maxlength' => 10,
      '#default_value' => isset($txn->points) ? $txn->points : 0,
      '#description' => t('The number of !points to add or subtract.  For example, enter %positive to add !points or %negative to deduct !points.', array('%positive' => 25, '%negative' => -25) + nodepoints_translation()),
      '#required' => TRUE,
      '#weight' => -15,
  );

  if (module_exists('taxonomy')) {
    $options = nodepoints_get_categories();
    $form['tid'] = array(
      '#type' => 'select',
      '#title' => t('Category'),
      '#default_value' => isset($txn->tid) ? $txn->tid : nodepoints_get_default_tid(),
      '#options' => $options,
      '#description' => t('The !points category that should apply to this transaction.', nodepoints_translation()),
      '#weight' => 0,
      // Only show the category if there are actually categories to choose from.
      '#access' => count($options) > 1,
    );
  }

  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['status'] = array(
    '#type' => 'fieldset',
    '#title' => t('Status'),
    '#group' => 'additional_settings',
  );

  if ($mode == 'add') {
    $form['status']['moderate'] = array(
      '#title' => t('Moderated'),
      '#type' => 'checkbox',
      '#description' => t('If checked, this !points transaction must be approved, through the moderation process.', nodepoints_translation()),
      '#default_value' => variable_get(NODEPOINTS_POINTS_MODERATION, 0),
      '#access' => nodepoints_admin_access('moderate'),
      '#weight' => -10,
    );
  }
  else {
    $form['status']['status'] = array(
      '#title' => t('Approval status'),
      '#type' => 'radios',
      '#options' => nodepoints_txn_status(),
      '#description' => t('Approval status of the transaction.'),
      '#default_value' => $txn->status,
      '#access' => nodepoints_admin_access('moderate'),
      '#weight' => -10,
    );
  }

  $form['status']['time_stamp'] = array(
    '#type' => 'textfield',
    '#title' => t('Date/Time'),
    '#default_value' => $timestamp,
    '#size' => 30,
    '#maxlength' => 30,
    '#description' => t('The date and time recorded for this transaction. Use this format: YYYY-MM-DD HH:MM +ZZZZ.'),
    '#weight' => -5,
    // Do not show this if it is not allowed to change the timestamp anyway.
    '#access' => !variable_get(NODEPOINTS_TRANSACTION_TIMESTAMP, 1),
  );

  $expirydate = 0;
  if (isset($txn->txn_id)) {
    if ($txn->expirydate > 0) {
      $expirydate = format_date($txn->expirydate, 'custom', 'Y-m-d H:i:s O');
    }
  }
  else {
    // If we're not editing we use site defaults.
    $expirydate = nodepoints_get_default_expiry_date();
    if ($expirydate) {
      $expirydate = format_date($expirydate, 'custom', 'Y-m-d H:i:s O');
    }
  }
  $form['status']['expirydate'] = array(
      '#type' => 'textfield',
      '#title' => t('Expiration date'),
      '#default_value' => $expirydate ? $expirydate : '',
      '#size' => 30,
      '#maxlength' => 30,
      '#description' => t('The date and time that the !points should expire. Use this format: YYYY-MM-DD HH:MM +ZZZZ. Leave this field blank if the !points should never expire.', nodepoints_translation()),
      '#weight' => 25,
  );

  $form['reason'] = array(
    '#type' => 'fieldset',
    '#title' => t('Reason'),
    '#group' => 'additional_settings',
  );

  $form['reason']['operation'] = array(
      '#type' => 'textfield',
      '#title' => t('Operation'),
      '#default_value' => isset($txn->operation) ? $txn->operation : t('admin'),
      '#maxlength' => 48,
      '#description' => t('The operation type for this transaction (default is %admin). Any value is valid but using a defined operation will cause an auto-generated description (specific to the chosen operation) to be included. This description can be translated into multiple languages.', array('%admin' => t('admin'))),
      '#weight' => 5,
      '#required' => FALSE,
      '#autocomplete_path' => 'nodepoints/operation-autocomplete',
  );

  $form['reason']['description'] = array(
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#default_value' => isset($txn->description) ? $txn->description : '',
      '#rows' => 7,
      '#cols' => 40,
      '#description' => t('Enter an optional description for this transaction, such as the reason !points were added or subtracted.', nodepoints_translation()),
      '#weight' => 10,
  );

  $form['reference'] = array(
    '#type' => 'fieldset',
    '#title' => t('Reference'),
    '#group' => 'additional_settings',
  );

  $options = array('' => '< ' . t('None') . ' >');
  foreach (entity_get_info() as $type => $info) {
    $options[$type] = $info['label'];
  }
  $form['reference']['entity_type'] = array(
    '#type' => 'select',
    '#title' => t('Linked entity'),
    '#weight' => 0,
    '#options' => $options,
    '#default_value' => isset($txn->entity_type) ? $txn->entity_type : '',
  );

  $form['reference']['entity_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Entity ID'),
    '#title_display' => 'invisible',
    '#weight' => 5,
    '#size' => 5,
    '#maxlength' => 20,
    '#default_value' => isset($txn->entity_id) ? $txn->entity_id : '',
    '#description' => t('Choose the entity type and ID to be referenced. A link to the entity will be shown.'),
  );

  $form['reference']['reference'] = array(
      '#type' => 'textfield',
      '#title' => t('Internal reference'),
      '#default_value' => isset($txn->reference) ? $txn->reference : '',
      '#size' => 30,
      '#maxlength' => 128,
      '#description' => t('Enter an optional reference code for this transaction. This is for internal tracking and is not shown to the end user.', nodepoints_translation()),
      '#weight' => 10,
  );

  $approved_by = !empty($txn->approver_uid) ? user_load($txn->approver_uid) : NULL;
  if ($approved_by) {
    $form['status']['approver'] = array(
      '#type' => 'textfield',
      '#title' => t('Moderator'),
      '#default_value' => $approved_by->name,
      '#size' => 30,
      '#maxlength' => 30,
      '#description' => t('The user who gave the transaction its current status.'),
      '#weight' => 30,
    );
  }

  $form['mode'] = array(
      '#type' => 'hidden',
      '#default_value' => $mode
  );

  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#weight' => 50,
  );
  return $form;
}
/**
 * Autocomplete callback for search an operation.
 *
 * @param $search
 *   Search string.
 */
function nodepoints_operation_autocomplete($search) {
  $results = array();
  if (!empty($search)) {
    foreach (nodepoints_get_info() as $operation => $info) {
      if (strpos($operation, $search) !== FALSE) {
        $results[$operation] = nodepoints_create_operation_autocomplete_label($operation, $info, $search);
      }
      elseif (isset($info['admin description']) && strpos($info['admin description'], $search) !== FALSE) {
        $results[$operation] = nodepoints_create_operation_autocomplete_label($operation, $info, $search);
      }
      elseif (isset($info['description']) && strpos($info['description'], $search) !== FALSE) {
        $results[$operation] = nodepoints_create_operation_autocomplete_label($operation, $info, $search);
      }
    }
  }
  drupal_json_output((object)$results);
}

/**
 * Create a autocomplete label with highlighted search text.
 *
 * @param $operation
 *   Operation string.
 * @param $info
 *   Operation info array.
 * @param $search
 *   Search text that will be highlighted if found.
 *
 * @return
 *   Highlighted label. If existing, the admin description is used and if not,
 *   the operation string.
 */
function nodepoints_create_operation_autocomplete_label($operation, $info, $search) {
  $label = $operation;
  if (!empty($info['admin description'])) {
    $label = $info['admin description'];
  }
  return preg_replace("/(" . preg_quote($search) . ")/i","<strong>$1</strong>", $label);
}

/**
 * Validate function for nodepoints transaction form.
 */
function nodepoints_admin_txn_validate($form, &$form_state) {
  $txn_node = node_load($form_state['values']['txn_node']);
  if (!is_object($txn_node)) {
    form_set_error('txn_node', t('Specified node does not exist.'));
  }
  else {
    form_set_value($form['txn_node'], $txn_node, $form_state);
  }

  if ((int)$form_state['values']['points'] == 0) {
    form_set_error('points', t('Amount of !points must be a positive or negative number.', nodepoints_translation()));
  }

  if (!strtotime($form_state['values']['time_stamp'])) {
    form_set_error('time_stamp', t('The provided timestamp is not a valid date.'));
  }

}

/**
 * Submit function for nodepoints transaction form.
 */
function nodepoints_admin_txn_submit($form, &$form_state) {
  global $user;
  if ($form_state['values']['mode'] == 'add') {
    $params = array(
      'points' => $form_state['values']['points'],
      'nid' => $form_state['values']['txn_node']->nid,
      'operation' => $form_state['values']['operation'],
      'description' => $form_state['values']['description'],
      'reference' => $form_state['values']['reference'],
      'tid' => $form_state['values']['tid'],
      'time_stamp' => strtotime($form_state['values']['time_stamp']),
      'moderate' => (bool)$form_state['values']['moderate'],
      'approver_uid' => $user->uid,
    );
    if ($form_state['values']['expirydate']) {
      // Check for the existence of an expirydate.
      $params['expirydate'] = strtotime($form_state['values']['expirydate']);
    }
    if (!empty($form_state['values']['entity_id']) && !empty($form_state['values']['entity_type'])) {
      $params['entity_type'] = $form_state['values']['entity_type'];
      $params['entity_id'] = (int)$form_state['values']['entity_id'];
    }
  }
  else {
    $expirydate = 0;
    if (!empty($form_state['values']['expirydate'])) {
      $expirydate = strtotime($form_state['values']['expirydate']);
    }

    // If status changed, the current user is the new approver, when not
    // changed, then the current approver is kept.
    if ($form_state['values']['txn']->status == $form_state['values']['status']) {
      $approver_uid = $form_state['values']['txn']->approver_uid;
    }
    else {
      $approver_uid = $user->uid;
    }

    $params = array(
      'nid' => $form_state['values']['txn']->nid,
      'approver_uid' => $approver_uid,
      'points' => $form_state['values']['points'],
      'tid' => $form_state['values']['tid'],
      'time_stamp' => strtotime($form_state['values']['time_stamp']),
      'operation' => $form_state['values']['operation'],
      'description' => $form_state['values']['description'],
      'reference' => $form_state['values']['reference'],
      'status' => $form_state['values']['status'],
      'expirydate' => $expirydate,
      'txn_id' => $form_state['values']['txn']->txn_id,
      'display' => FALSE,
    );
    if (!empty($form_state['values']['entity_id']) && !empty($form_state['values']['entity_type'])) {
      $params['entity_type'] = $form_state['values']['entity_type'];
      $params['entity_id'] = (int)$form_state['values']['entity_id'];
    }
    drupal_set_message(t('Changes to the !points transaction have been saved.', nodepoints_translation()));
  }
  nodepoints_nodepointsapi($params);

  $form_state['redirect'] = 'admin/config/content/nodepoints';
}

/**
 * Provides an administrative interface for managing points.
 */
function nodepoints_admin_points($form, &$form_state) {

  // If this is an AJAX request, update $_GET['q'] so that table sorting and
  // similar links are using the correct base path.
  if ($_GET['q'] == 'system/ajax') {
    $_GET['q'] = 'admin/config/content/nodepoints';
  }

  $header = nodepoints_get_list_header();
  $query = db_select('nodepoints', 'p')->extend('PagerDefault')->extend('TableSort')
    ->fields('p', array('nid', 'points', 'tid'))
    ->fields('n', array('title'))
    ->groupBy('p.nid')
    ->groupBy('n.title')
    ->groupBy('p.points')
    ->groupBy('p.tid')
    ->orderByHeader($header)
    ->limit(variable_get(NODEPOINTS_REPORT_NODECOUNT, 30));

  $query->join('node', 'n', 'p.nid = n.nid');
  if (module_exists('taxonomy')) {
    $query->groupBy('t.name');
    $query->leftJoin('taxonomy_term_data', 't', 'p.tid = t.tid');
  }

  $values = nodepoints_filter_parse_input($form_state);
  $active_category = nodepoints_filter_query($query, $values);

  if (isset($active_category)) {
    drupal_set_title(t('Totals (%category category)', nodepoints_translation() + array('%category' => $active_category)), PASS_THROUGH);
  }
  else {
    drupal_set_title(t('Totals'));
  }

  if (variable_get(NODEPOINTS_REPORT_DISPLAYZERO, 1) == 0) {
    // The user would NOT like to see nodes with zero points.
    $query->condition('p.points', 0, '<>');
  }

  $rows = array();
  foreach ($query->execute() as $data) {
    $rows[] = nodepoints_get_list_row($data);
  }

  $output = array();
  $output['form'] = nodepoints_filter_form(NULL, $values);
  $output['list'] = array(
    '#type' => 'container',
    '#id' => 'nodepoints_list_wrapper',
  );
  $output['list']['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
  $output['list']['pager'] = array(
    '#theme' => 'pager',
  );
  return $output;
}


/**
 * Displays a list of transactions.
 *
 */
function nodepoints_admin_transactions($form, &$form_state, $moderate) {

  // If this is an AJAX request, update $_GET['q'] so that table sorting and
  // similar links are using the correct base path.
  if ($_GET['q'] == 'system/ajax') {
    $_GET['q'] = $moderate ? 'admin/config/content/nodepoints/moderate' : 'admin/config/content/nodepoints/transaction';
  }

  $settings = array(
    'show_status' => !$moderate,
  );
  $header = nodepoints_get_transaction_header($settings);
  $query = db_select('nodepoints_txn', 'p')->extend('PagerDefault')->extend('TableSort')
    ->fields('p')
    ->orderByHeader($header)
    // Enforce consistent sort order.
    ->orderBy('p.txn_id', 'DESC')
    ->limit(variable_get(NODEPOINTS_REPORT_NODECOUNT, 30));

  if ($moderate) {
    $query->condition('p.status', NODEPOINTS_TXN_STATUS_PENDING);
  }

  if (module_exists('taxonomy')) {
    $query->leftJoin('taxonomy_term_data', 't', 'p.tid = t.tid');
  }

  $values = nodepoints_filter_parse_input($form_state);
  $active_category = nodepoints_filter_query($query, $values);

  if ($moderate) {
    if (isset($active_category)) {
      drupal_set_title(t('Moderation (%category category)', nodepoints_translation() + array('%category' => $active_category)), PASS_THROUGH);
    }
    else {
      drupal_set_title(t('Moderation'));
    }
  }
  else {
    if (isset($active_category)) {
      drupal_set_title(t('Transactions (%category category)', nodepoints_translation() + array('%category' => $active_category)), PASS_THROUGH);
    }
    else {
      drupal_set_title(t('Transactions'));
    }
  }

  $rows = array();
  foreach ($query->execute() as $transaction) {
    $rows[] = nodepoints_get_transaction_row($transaction, $settings);
  }

  // Store context in the output array so that modules have access to it.
  $output = array(
    '#attached' => array(
      'css' => array(
        drupal_get_path('module', 'nodepoints') . '/nodepoints.css',
      ),
    ),
  );
  $output['form'] = nodepoints_filter_form(NULL, $values);

  $output['list'] = array(
    '#type' => 'container',
    '#id' => 'nodepoints_list_wrapper',
  );

  $output['list']['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => $moderate ? t('No !points awaiting moderation', nodepoints_translation()) : t('No !Points earned', nodepoints_translation()),
    '#weight' => -5,
    '#attributes' => array('class' => array($moderate ? 'nodepoints-moderation-list' : 'nodepoints-transactions-list')),
  );
  $output['list']['pager'] = array(
    '#theme' => 'pager',
    '#weight' => 0,
  );

  return $output;
}

function nodepoints_confirm_approve($form, $form_state, $operation, $transaction) {
  $form = array(
    'txn_id' => array(
        '#type' => 'value',
        '#value' => $transaction->txn_id,
    ),
  );

  $arguments = array(
    '!node' => $transaction->node->title,
    '%category' => $transaction->category,
  ) + nodepoints_translation();

  if ($operation == 'approve') {
    $question = t('Approve transaction');
    $description = format_plural($transaction->points, 'Do you want to approve @count !point for !node in the %category category?', 'Do you want to approve @count !points for !node in the %category category?', $arguments);
    $form['operation'] = array(
      '#type' => 'value',
      '#value' => NODEPOINTS_TXN_STATUS_APPROVED,
    );
  }
  else {
    $question = t('Decline transaction');
    $description = format_plural($transaction->points, 'Do you want to decline @count !point for !node in the %category category?', 'Do you want to decline @count !points for !node in the %category category?', $arguments);
    $form['operation'] = array(
      '#type' => 'value',
      '#value' => NODEPOINTS_TXN_STATUS_DECLINED,
    );
  }

  $description = '<p><strong>' . $description . '</strong></p>';
  $description .= '<p>' . t('Reason: !reason', array('!reason' => nodepoints_create_description($transaction))) . '</p>';

  return confirm_form($form, $question, 'admin/config/content/nodepoints/moderate', $description);
}
/**
 * Menu callback for settings form.
 */
function nodepoints_admin_settings($form, &$form_state) {

  drupal_set_title(t('!Points settings', nodepoints_translation()));

  drupal_add_js(drupal_get_path('module', 'nodepoints') . '/nodepoints_admin.js');

  $form['settings'] = array(
    '#prefix' => '<h3>' . t('Core !points settings', nodepoints_translation()) . '</h3>',
    '#type' => 'vertical_tabs',
  );
  $group = 'renaming';
  $form[$group] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Branding'),
      '#group' => 'settings',
  );

  $form[$group][NODEPOINTS_TRANS_UCPOINTS] = array(
      '#type' => 'textfield',
      '#title' => t('Word to use in the interface for the upper case plural word !Points', nodepoints_translation()),
      '#default_value' => variable_get(NODEPOINTS_TRANS_UCPOINTS, 'Points'),
      '#size' => 20,
      '#maxlength' => 20,
  );

  $form[$group][NODEPOINTS_TRANS_LCPOINTS] = array(
      '#type' => 'textfield',
      '#title' => t('Word to use in the interface for the lower case plural word !points', nodepoints_translation()),
      '#default_value' => variable_get(NODEPOINTS_TRANS_LCPOINTS, 'points'),
      '#size' => 20,
      '#maxlength' => 20,
  );
  $form[$group][NODEPOINTS_TRANS_UCPOINT] = array(
      '#type' => 'textfield',
      '#title' => t('Word to use in the interface for the upper case singular word !Point', nodepoints_translation()),
      '#default_value' => variable_get(NODEPOINTS_TRANS_UCPOINT, 'Point'),
      '#size' => 20,
      '#maxlength' => 20,
  );
  $form[$group][NODEPOINTS_TRANS_LCPOINT] = array(
      '#type' => 'textfield',
      '#title' => t('Word to use in the interface for the lower case singular word !point', nodepoints_translation()),
      '#default_value' => variable_get(NODEPOINTS_TRANS_LCPOINT, 'point'),
      '#size' => 20,
      '#maxlength' => 20,
  );

  $group = 'status';
  $form[$group] = array(
      '#type' => 'fieldset',
      '#title' => t('Moderation'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => -1,
      '#group' => 'settings',
  );

  $form[$group][NODEPOINTS_POINTS_MODERATION] = array(
      '#type' => 'radios',
      '#title' => t('Transaction status'),
      '#default_value' => variable_get(NODEPOINTS_POINTS_MODERATION, 0),
      '#options' => array(t('Approved'), t('Moderated')),
      '#description' => t('Select whether all !points should be approved automatically, or moderated, and require admin approval', nodepoints_translation()),
  );

  $group = "Points expiration";
  $form[$group] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Expiration', nodepoints_translation()),
      '#description' => t('These settings affect new !points only, they are not retroactive. !Points expiration depends upon cron.', nodepoints_translation()),
      '#group' => 'settings',
  );

  $form[$group][NODEPOINTS_EXPIREAFTER_DATE] = array(
      '#type' => 'select',
      '#title' => t('Expire !points after', nodepoints_translation()),
      '#description' => t('Once !points have been obtained by the node
                          they will expire according to this setting', nodepoints_translation()),
      '#options' => nodepoints_expiry_dates(),
      '#default_value' => variable_get(NODEPOINTS_EXPIREAFTER_DATE, NULL),
  );

  // If the expiration date is earlier than today/ new points will last forever.
  // Although this may be desirable/ it could also be an oversight so we'll
  // display a message to the administrator.
  $warning = "";
  if (nodepoints_date_to_timestamp(variable_get(NODEPOINTS_EXPIREON_DATE, array('day' => 1, 'month' => 1, 'year' => 1900))) < REQUEST_TIME) {
    $warning = '<br /><strong>' . t('This setting will not take affect, date must be in the future') . '</strong>';
  }

  $form[$group][NODEPOINTS_EXPIREON_DATE] = array(
      '#type' => 'date',
      '#title' => t('Expire !points on this date', nodepoints_translation()),
      '#description' => t('Once !points have been obtained by the node they will
                         last until this date. This setting overrides the
                         "Expire after setting" above ', nodepoints_translation()) . $warning,
      '#default_value' => variable_get(NODEPOINTS_EXPIREON_DATE, array('day' => 1, 'month' => 1, 'year' => 1980)),
  );
	
  $group = "reports";
  $form[$group] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Listings'),
      '#group' => 'settings',
  );

  $form[$group][NODEPOINTS_REPORT_LIMIT] = array(
      '#type' => 'select',
      '#title' => t('Transactions per page'),
      '#default_value' => variable_get(NODEPOINTS_REPORT_LIMIT, 10),
      '#options' => array(10 => 10, 20 => 20, 30 => 30, 40 => 40, 50 => 50, 100 => 100),
      '#description' => t('Limits the number of transactions displayed per page.'),
  );
  $form[$group][NODEPOINTS_REPORT_DISPLAYZERO] = array(
      '#type' => 'radios',
      '#title' => t('Display zero !point nodes?', nodepoints_translation()),
      '#default_value' => variable_get(NODEPOINTS_REPORT_DISPLAYZERO, 1),
      '#options' => array(t('No'), t('Yes')),
      '#description' => t('If set to "No" nodes with zero !points will not be displayed in the reports', nodepoints_translation()),
  );
  $form[$group][NODEPOINTS_REPORT_NODECOUNT] = array(
      '#type' => 'select',
      '#title' => t('Nodes per page'),
      '#default_value' => variable_get(NODEPOINTS_REPORT_NODECOUNT, 30),
      '#options' => array(10 => 10, 20 => 20, 30 => 30, 40 => 40, 50 => 50, 100 => 100),
      '#description' => t('When listing !points by node limit how many nodes are displayed on a single page', nodepoints_translation()),
  );

  $form[$group]['nodepoints_truncate'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Truncation length for the "Reason" column in transaction listings'),
    '#description'   => t('Choose the truncation length in characters for the "Reason" column in transaction listings. The reason is not truncated on the transaction details page.'),
    '#default_value' => variable_get('nodepoints_truncate', 30),
    '#size'          => 5,
    '#maxlength'     => 5,
  );

  // Categories will only appear if the taxonomy module is enabled as
  // the module is required for this functionality but not necessarily
  // a requirement for the module.
  if (module_exists('taxonomy')) {
    $group = 'category';
    $form[$group] = array(
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#title' => t('Categorization', nodepoints_translation()),
      '#group' => 'settings',
    );
    $form[$group][NODEPOINTS_CATEGORY_DEFAULT_TID] = array(
        '#type' => 'select',
        '#title' => t('Default Category'),
        '#default_value' => variable_get(NODEPOINTS_CATEGORY_DEFAULT_TID, NULL),
        '#options' => nodepoints_get_categories(),
        '#description' => t('By default all !points are assigned to this category.  You can modify what categories are available by modifying the <a href="!url">Nodepoints taxonomy</a>',
                array_merge(nodepoints_translation(), array('!url' =>  url('admin/structure/taxonomy/' . taxonomy_vocabulary_load(variable_get(NODEPOINTS_CATEGORY_DEFAULT_VID, ''))->machine_name)))),
    );
    $options = nodepoints_get_categories(NULL);
    // 0 can not be used as a checkbox value.
    $options = array('uncategorized' => $options[0]) + $options + array('all' => t('Total !points in all categories', nodepoints_translation()));
    unset($options[0]);
    $form[$group][NODEPOINTS_CATEGORY_PROFILE_DISPLAY_TID] = array(
        '#type' => 'checkboxes',
        '#title' => t("Categories to display on the node page and in the Node's !points block", nodepoints_translation()),
        '#default_value' => variable_get(NODEPOINTS_CATEGORY_PROFILE_DISPLAY_TID, array_keys($options)),
        '#options' => $options,
        '#description' => t('Select the !points categories that should be displayed. Check "Total !points in all categories" to display a sum total of all individual !points categories.', nodepoints_translation()),
    );

    $form[$group][NODEPOINTS_TRANS_UNCAT] = array(
      '#type' => 'textfield',
      '#title' => t('Word to use for the general category'),
      '#default_value' => variable_get(NODEPOINTS_TRANS_UNCAT, 'General'),
      '#description' => t("By default, %default is the name used for the module's umbrella category. You may change this here.", array('%default' => t('General'))),
      '#size' => 20,
      '#maxlength' => 20,
    );
  }
  // New configuration options to override current timestamp.
  $group = "stamping";
  $form[$group] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Transaction stamping'),
      '#group' => 'settings',
  );
  $form[$group][NODEPOINTS_TRANSACTION_TIMESTAMP] = array(
      '#type' => 'checkbox',
      '#title' => t('Always use system time'),
      '#default_value' => variable_get(NODEPOINTS_TRANSACTION_TIMESTAMP, 1),
      '#description' => t('Sets if the transaction timestamp should obey current time, or can be modified by the API operations. Unchecking this option will allow customization of timestamp for the transactions.'),
  );

  $form['settings_additional'] = array(
    '#prefix' => '<h3>' . t('Additional !points settings', nodepoints_translation()) . '</h3>',
    '#type' => 'vertical_tabs',
  );

  $form['setting'] = nodepoints_invoke_all('setting');

  // Hide the additional vertical_tabs element if nothing is being displayed in
  // it.
  if (isset($form['settings']) && empty($form['settings'])) {
    $form['settings_additional']['#access'] = FALSE;
  }

  return system_settings_form($form);
}
